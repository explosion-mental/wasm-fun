const std = @import("std");

var buffer: [720 * 480]u32 = undefined;
var time: u64 = 0;

pub extern "surface" fn blit() callconv(.C) void;
pub extern "surface" fn init(p: usize) callconv(.C) void;

export fn setup() void {
    std.mem.set(u32, &buffer, 0xffffffff);
    init(@ptrToInt(&buffer));
}

export fn step() bool {
    for (buffer) |*p| p.* -%= 0xff;
    blit();

    return true;
}
